package ejercicios.Pildoras;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class EjercicioClase146 {
    public static void main(String[] args) {
        EjercicioClase146 programa = new EjercicioClase146();
        programa.inici();
    }

    public void inici(){

        Scanner lector = new Scanner(System.in);
        System.out.println("Introduce el nombre del directorio, por favor");
        String nombre_directorio = lector.nextLine();
        System.out.println("Introduce el nombre del archivo, por favor");
        String nombre_archivo = lector.nextLine();
        System.out.println("Introduce la frase, por favor");
        String frase = lector.nextLine();
        //C:\Users\Usuario\Desktop\Programación web\CarpetaPrueba
        File directorio = new File("C:" + File.separator + "Users"+ File.separator + "Usuario"+File.separator+"Desktop"+File.separator+"Programación web"+File.separator+"CarpetaPrueba"+File.separator+nombre_directorio);

        System.out.println(directorio.exists());

        directorio.mkdir();

        System.out.println(directorio.exists());

        File ruta_archivo = new File("C:" + File.separator + "Users"+ File.separator + "Usuario"+File.separator+"Desktop"+File.separator+"Programación web"+File.separator+"CarpetaPrueba"+File.separator+nombre_directorio + File.separator + nombre_archivo + ".txt");

        try{
            ruta_archivo.createNewFile();
            FileWriter escritura = new FileWriter("C:" + File.separator + "Users"+ File.separator + "Usuario"+File.separator+"Desktop"+File.separator+"Programación web"+File.separator+"CarpetaPrueba"+File.separator+nombre_directorio + File.separator + nombre_archivo + ".txt", true);

            for (int i=0; i<frase.length();i++){
                escritura.write(frase.charAt(i));
            }

            escritura.close();
        }catch (Exception e){

        }
    }
}
