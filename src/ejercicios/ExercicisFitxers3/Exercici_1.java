package ejercicios.ExercicisFitxers3;

import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;

/**
 @description: it asks you on screen for the students notes and their names, then shows you on the screen their average grades
 @author: Adrian Antanyon
 @version: 11/05/2020
 */
public class Exercici_1 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        Exercici_1 programa = new Exercici_1();
        programa.inici();
    }

    public void inici(){
        File notas_alumnos = new File("notas_alumnos.txt");
        System.out.println("-----  BIENVENIDO AL GESTOR DE ALUMNOS  -----\n");
        try {
            PrintStream notas = new PrintStream(notas_alumnos);
            introducirNotasAlumnos(notas);

        }catch (Exception e){
            System.out.println("Error: " + e);
        }

        imprimirMediaAlumnos(notas_alumnos);


    }

    public void introducirNotasAlumnos(PrintStream notas_alumnos){

        String [] datos_alumno = {"nombre","apellido"};
        boolean menuEncendido = true;
        String finalizar_programa;
        int contador = 0;
        do {
            System.out.println("Introduzca los datos del alumno, si este es el último alumno escriba FI ahora y después de introducir sus datos el programa finalizará, si no escriba lo que quiera.");
            finalizar_programa = lector.next().toLowerCase();
            lector.nextLine();
            if(finalizar_programa.equals("fi")){
                menuEncendido = false;
            }

            for (int i = 0; i < datos_alumno.length; i++) {
                System.out.println("Introduce el "+datos_alumno[i]+" del alumno, por favor");
                String nombre_y_apellido = lector.next();
                lector.nextLine();
                notas_alumnos.print(nombre_y_apellido + " ");
            }

            escrituraNotas(notas_alumnos);

            notas_alumnos.println();
        }while (menuEncendido);

        notas_alumnos.close();
    }

    public void escrituraNotas(PrintStream notas_alumnos){
        System.out.println("Cuántas notas le gustaría introducir, por favor?");
        comprobacionInt();
        int cantidad_notas = lector.nextInt();
        double [] notas = new double[cantidad_notas];

        for (int i=0; i<cantidad_notas;i++){
            System.out.println("Introduzca la nota " +(i+1)+", por favor");
            comprobacionDouble();
            double nota = lector.nextDouble();
            notas[i] = nota;
            if (nota < 0 || nota > 10){
                System.out.println("La nota no puede ser menor que 0 o mayor que 10, vuelva a introducirla, por favor");
                comprobacionDouble();
                nota = lector.nextDouble();
            }
            notas_alumnos.print(nota + " ");
        }
    }

    public void comprobacionInt(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número válido, vuelva a introducirlo, por favor");
            lector.next();
        }
    }

    public void comprobacionDouble(){
        while (!lector.hasNextDouble()){
            System.out.println("No es un número válido, vuelva a introducirlo, por favor");
            lector.next();
        }
    }

    public void imprimirMediaAlumnos(File datos_alumnos){
        int i,j;
        double nota, media;
        try {
            Scanner lectura_datos_alumnos = new Scanner(datos_alumnos);
            System.out.println("La nota media de los alumnos es: ");
            do {
                i=0;
                j=0;
                nota = 0;
                while (j < 2){
                    String dato_del_fichero = lectura_datos_alumnos.next();
                    System.out.print(dato_del_fichero + " ");
                    j++;
                }
                while (!lectura_datos_alumnos.hasNextLine()){
                    nota += lectura_datos_alumnos.nextDouble();
                    i++;
                }

                media = nota/i;
                System.out.println(media);
            }while (lectura_datos_alumnos.hasNext());

        }catch (Exception e){
            System.out.println("Error: " + e);
        }
    }
}
