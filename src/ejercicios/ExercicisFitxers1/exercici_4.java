package ejercicios.ExercicisFitxers1;

import java.io.File;
import java.util.Scanner;

/**
 @description: Reads a direction and show you by screen the files of 50Kb or more it has inside.
 @author: Adrian Antanyon
 @version: 23/04/2020
 */
public class exercici_4 {
    final int PESO_50KB = 512000;
    public static void main(String[] args) {
        exercici_4 programa = new exercici_4();

        programa.inici();
    }

    public void inici(){

        String ruta = solicitarRuta();

        mostrarArchivos(ruta);

    }

    public String solicitarRuta(){
        Scanner lector = new Scanner(System.in);

        System.out.println("Introduce la ruta de la carpeta donde quieres buscar, por favor");
        String ruta = lector.nextLine();

        return ruta;
    }

    public void mostrarArchivos(String ruta){
        File carpeta = new File(ruta);

        System.out.println("El nombre de la carpeta es " + carpeta.getName()+"\n" +
                "La ruta absoluta que usaremos es " + carpeta.getAbsolutePath()+"\n" +
                "Procedemos a comprobar su existencia\n" +
                "BUSCANDO...\n" +
                "BUSCANDO...\n" +
                "BUSCANDO...");

        if (carpeta.exists()){
            System.out.println("Carpeta encontrada");
            System.out.println("Y tiene los siguientes archivos que pesan más de 49 Kb's ");
            File [] contenidoCarpeta = carpeta.listFiles();

            for (int i=0;i<contenidoCarpeta.length;i++){
                if (contenidoCarpeta[i].isFile()){
                    long longitudArchivo = contenidoCarpeta[i].length();
                    if (longitudArchivo >= PESO_50KB){
                        System.out.println(contenidoCarpeta[i].getName() + " con  un peso de " + (longitudArchivo/1024)+" Kb's");
                    }
                }
            }
        }else{
            System.out.println("Lamentablemente la carpeta no se ha encontrado.");
        }

    }
}
