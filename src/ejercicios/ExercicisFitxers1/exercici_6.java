package ejercicios.ExercicisFitxers1;

import java.io.File;
import java.util.Date;
import java.util.Scanner;

/**
 @description: It's a file path manager with 2 new options
 @author: Adrian Antanyon
 @version: 24/04/2020
 */
public class exercici_6 {
    Scanner lector = new Scanner(System.in);


    public static void main(String[] args) {
        exercici_6 programa = new exercici_6();
        programa.inici();
    }

    public void inici(){
        File carpeta = new File("C:\\Users\\Usuario\\IdeaProjects\\UF3_ficheros");
        boolean menuEncendido = true;
        String seleccion;
        System.out.println("Bienvenido al gestor de ficheros por pantalla");
        System.out.println("Actualmente se encuentra en " + carpeta.getAbsolutePath());
        do {
            menu();
            seleccion = lector.nextLine().toLowerCase();
            switch (seleccion){
                case "cd":
                    carpeta=cd(carpeta);
                    break;
                case "ver":
                    carpeta=entrarCarpeta(carpeta);
                    break;
                case "dir":
                    verArchivosDirectorio(carpeta);
                    break;
                case "del":
                    borrarArchivo(carpeta);
                    break;
                case "prop":
                    mostrarPropiedades(carpeta);
                    break;
                case "fi":
                case "fin":
                    System.out.println("Saliendo del gestor de archivos, que pase un buen día!");
                    menuEncendido = false;
                    break;
                default:
                    System.out.println("Comando inválido.");
            }
        }while(menuEncendido);
    }

    public void menu(){
        System.out.println("Seleccione la opción que desea, por favor:\n" +
                "CD --> ir a la carpeta padre\n" +
                "VER --> entras en una carpeta que esté dentro de la carpeta actual\n" +
                "DIR -->  muestra todos los archivos y carpetas dentro de la actual\n" +
                "DEL --> borra un archivo de la carpeta actual\n" +
                "PROP --> muestra el tamaño y última fecha de modificación de un archivo\n" +
                "FIN --> finalizar el programa");
    }

    public File cd(File carpeta){
        try{
            System.out.println("\nAccediendo a la carpeta padre");
            carpeta = new File(carpeta.getParent());
            System.out.println("Ahora se encuentra en " + carpeta.getAbsolutePath()+"\n");
        }catch (Exception e){
            System.out.println("No es posible acceder más atrás de " + carpeta.getName()+"\n" +
                    "Salta el siguiente error: " + e +"\n");
        }

        return carpeta;
    }

    public File entrarCarpeta(File carpeta){
        String [] nombres = carpeta.list();

        System.out.println("\nEstas son las carpetas pertenecientes a " + carpeta.getName() + " y que puede acceder\n");
        for (int i=0;i<nombres.length;i++){
            File carpetaInterna = new File(carpeta.getAbsolutePath(), nombres[i]);
            if (carpetaInterna.isDirectory()){
                System.out.println(nombres[i]);
            }
        }

        System.out.println("Escriba el nombre exactamente igual para introducirte en ella, por favor");
        String accesoCarpeta = lector.nextLine();
        carpeta = new File(carpeta.getAbsolutePath() +"\\"+ accesoCarpeta);

        System.out.println("Introduciendose en " + carpeta.getAbsolutePath()+"\n");

        return carpeta;
    }

    public void verArchivosDirectorio(File carpeta){
        String [] nombres = carpeta.list();
        System.out.println("\nVisualizando archivos y carpetas de " + carpeta.getName()+"\n");

        for (String lista: nombres){
            System.out.println(lista);
        }

        System.out.println("\nRegresando al menú\n");
    }

    public boolean listarArchivos(File carpeta){
        String [] nombres = carpeta.list();
        boolean existe = false;

        System.out.println("La carpeta " + carpeta.getName() + " dispone de los siguientes archivos\n");

        for (int i=0;i<nombres.length;i++){
            File carpetaInterna = new File(carpeta.getAbsolutePath(), nombres[i]);
            if (carpetaInterna.isFile()){
                existe = true;
                System.out.println(nombres[i]);
            }
        }

        return existe;
    }

    public void borrarArchivo(File carpeta){

        boolean existe = listarArchivos(carpeta);
        if (existe){
            System.out.println("\nIntroduce el nombre y extensión del archivo que quieras eliminar, por favor");
            String archivoEliminar = lector.nextLine();
            File archivo = new File(archivoEliminar);
            if (archivo.exists()){
                archivo.delete();
                System.out.println(archivoEliminar + " eliminado\n");
            }else {
                System.out.println("El archivo introducido no existe, regresando al menú de selección\n");
            }

        }else {
            System.out.println("COMPROBANDO...\n" +
                    "COMPROBANDO...\n" +
                    "COMPROBANDO...\n" +
                    "No existen archivos dentro de la carpeta, regranso al menú de selección");
        }
    }

    public void mostrarPropiedades(File carpeta){
       boolean existe = listarArchivos(carpeta);
        if (existe){
            System.out.println("\nIntroduce el nombre y extensión del archivo que deseas saber sus propiedades, por favor");
            String archivoPropiedades = lector.nextLine();
            File archivo = new File(archivoPropiedades);
            if (archivo.exists()){
                System.out.print("El tamaño es de "+archivo.length()/1024+" Kb's\n" +
                        "Y la última modificación fue realizada el " );
                Date fecha_ultima_modificacion = new Date(archivo.lastModified());
                System.out.println(fecha_ultima_modificacion);
            }else {
                System.out.println("El archivo introducido no existe, regresando al menú de selección\n");
            }

        }else {
            System.out.println("COMPROBANDO...\n" +
                    "COMPROBANDO...\n" +
                    "COMPROBANDO...\n" +
                    "No existen archivos dentro de la carpeta, regranso al menú de selección");
        }
    }
}
