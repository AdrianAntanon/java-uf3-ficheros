package ejercicios.ExercicisFitxers1;

/**
    @description: Checks if a directory exists or not, so deletes it while exists and if not it makes it.
    @author: Adrian Antanyon
    @version: 15/04/2020
     */

import java.io.File;

public class exercici_1 {
    public static void main(String[] args) {
        exercici_1 programa = new exercici_1();
        programa.inici();
    }
    public void inici(){
        File temp = new File("Temp");

        comprobarExistenciaCarpeta(temp);

    }
    public void comprobarExistenciaCarpeta(File document){
        if (document.exists()){
            System.out.println("La carpeta " +document.getName() + " existe y la ruta absoluta es: " + document.getAbsolutePath());
            document.delete();
            System.out.println("Se procede a su eliminación.");
        }else {
            System.out.println("El archivo " + document.getName() +", no existe, por lo que se procede a su creación");
            document.mkdir();
            System.out.println("Carpeta creada, la ruta completa es " + document.getAbsolutePath());
        }
    }
}
