package ejercicios.ExercicisFitxers1;

import java.io.File;
import java.util.Scanner;

/**
 @description: Changes the extension of a file for another.
 @author: Adrian Antanyon
 @version: 21/04/2020
 */

public class exercici_2 {
    String path = "C:\\Users\\Usuario\\IdeaProjects\\UF3_ficheros";

    public static void main(String[] args) {
        exercici_2 programa = new exercici_2();
        programa.inici();
    }

    public void inici(){
        File carpeta = new File(path);
        System.out.println("La carpeta donde se va a buscar es la siguiente: \n" +
                carpeta.getAbsolutePath());
        System.out.println("Y hay los siguientes archivos en ella");
        mostrarCarpeta();
        pediarExtension();
        System.out.println("Ahora hay los siguientes archivos");
        mostrarCarpeta();
    }

    public void mostrarCarpeta(){
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++)         {
            if (listOfFiles[i].isFile())             {
                String files = listOfFiles[i].getName();
                System.out.println(files);
            }
        }
    }

    public void pediarExtension(){
        Scanner lector = new Scanner(System.in);
        System.out.println("Escribe la extensión de los archivos que quieras reemplazar y a cuál quieres convertirla, por favor \n" +
                "Ejemplo: pasar todos los archivos txt a jpg");
        String primeraExtension = lector.next();
        String segundaExtenson = lector.next();
        convertirArchivos(primeraExtension,segundaExtenson);
    }

    public void convertirArchivos(String extQuitar, String extPoner){
        System.out.println(extQuitar + " y " + extPoner);
        File carpeta = new File(path);
        String [] ficheros = carpeta.list();

        for (int i=0; i<ficheros.length;i++){
            int index = (ficheros[i].lastIndexOf("."))+1;
            String extension = ficheros[i].substring(index);
            String fichero = ficheros[i].substring(0,index);
            if (extension.equalsIgnoreCase(extQuitar)){
                File ruta = new File( path + "/"+fichero+extension);
                File nuevoNombre = new File(path+"/"+fichero+extPoner);
                boolean cambio = ruta.renameTo(nuevoNombre);
                if (cambio){
                    System.out.println("Archivo cambiado, ahora es " + nuevoNombre);
                }else{
                    System.out.println("No se cambia");
                }
            }
        }


    }
}
