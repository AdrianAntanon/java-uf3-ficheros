package ejercicios.FitxersText;

import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;

/**
 @description: Program similar than a writer machine, asks you a file name, checks if exists or not and saves all your stuff inside a file .TXT
 @author: Adrian Antanyon
 @version: 16/05/2020
 */
public class Ejercicio2_TEXT {
    public static void main(String[] args) {
        Ejercicio2_TEXT programa = new Ejercicio2_TEXT();

        programa.inici();
    }

    public void inici() {
        System.out.println("B I E N V E N I D O   A   L A   M A Q U I N A   D E   E S C R I B I R.\n" +
                "Este programa sirve para crear un archivo TXT y guardar todo lo que seas capaz de escribir en él.");
        try {
            File nombreArchivo = introducirNombreArchivo();
            PrintStream writer = new PrintStream(nombreArchivo);

            acabar(writer);

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }

    public void acabar(PrintStream writer){
        Scanner lector = new Scanner(System.in);

        boolean lecturaArchivo = true;
        while (lecturaArchivo) {
            String nuevaLinea = lector.nextLine();
            if (".".equals(nuevaLinea)) {
                lecturaArchivo = false;
                System.out.println("Cerrando la  M A Q U I N A   D E   E S C R I B I R.\n" +
                        "Que pase un buen día");
            } else {
                writer.println(nuevaLinea);
            }
        }

        writer.close();
    }

    public File introducirNombreArchivo() {
        Scanner lector = new Scanner(System.in);
        String TXT = ".txt";
        boolean preguntar = true;
        File archivo = null;
        while (preguntar) {
            System.out.println("Introduce el nombre del archivo donde le gustaría escribir, por favor: ");
            String nombreArchivo = lector.nextLine();
            archivo = new File(nombreArchivo+TXT);
            if (archivo.exists()) {
                System.out.println("El archivo introducido ya existe");
            } else {
                preguntar = false;
                System.out.println("Archivo creado, ahora escriba lo que quiera guardar, en caso de querer terminar introduzca el punto final en una línea única, por favor.");
            }
        }
        return archivo;
    }
}
