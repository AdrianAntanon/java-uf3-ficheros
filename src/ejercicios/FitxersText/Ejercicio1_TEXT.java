package ejercicios.FitxersText;

import java.io.File;
import java.util.Scanner;

/**
 @description: Reads a text file line by line, until the word "fin" is found or there are no more lines, shows you by screen the amount of words by line too.
 @author: Adrian Antanyon
 @version: 12/05/2020
 */
public class Ejercicio1_TEXT {
    public static void main(String[] args) {
        Ejercicio1_TEXT programa = new Ejercicio1_TEXT();

        programa.inici();
    }

    public void inici(){

        try {
            File palabrasTXT = new File("Palabras.txt");
            Scanner lector = new Scanner(palabrasTXT);

            contarPalabras(lector);

        } catch (Exception e) {
            System.out.println("Error inesperado: " + e);
        }

    }

    public void contarPalabras(Scanner lector){
        boolean seguirLeyendo = true;
        int linea = 1;
        String finalizar = "fin";

        do {
            String acabar = lector.nextLine().toLowerCase();
            if (finalizar.equals(acabar)){
                seguirLeyendo = false;
                System.out.println("La lectura del archivo ha terminado en la línea " + linea);
            }else {
                String[] palabras = acabar.split(" ");
                int cantidadPalabras = palabras.length;
                System.out.println("Línea: " + linea + " - Cantidad de palabras: " + cantidadPalabras);
                linea++;
            }
        }while (seguirLeyendo);

    }
}

