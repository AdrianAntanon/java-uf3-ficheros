package ejercicios.Recuperacion_Alex;

import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;

public class ejercicio_2 {
    Scanner lector = new Scanner(System.in);
    public static void main(String[] args) {
        ejercicio_2 programa = new ejercicio_2();

        programa.inici();
    }

    public void inici(){

        File info_persona = new File("info_persona.txt");
        boolean salir = false;
        int seleccion = 0;

        do {
            menu();
            seleccion = lector.nextInt();
            switch (seleccion){
                case 1:
                    info_persona=crearArchivo(info_persona);
                    break;
                case 2:
                    mostrarPorPantalla(info_persona);
                    break;
                case 3:
                    System.out.println("Saliendo del programa, que pase un buen día.");
                    salir = true;
                    break;
                default:
                    System.out.println("Opción no disponible en fase de desarrollo.");
            }

        }while (!salir);

    }

    public void menu(){
        System.out.println("Seleccione la opción que quiera realizar, por favor:\n" +
                "1) Crear archivo\n" +
                "2) Mostrar información por pantalla\n" +
                "3) Salir");
        comprobacionInt();
    }

    public File crearArchivo(File info_persona){
        try{
            info_persona.createNewFile();

            String [] preguntas = {"Nombre: ", "Apellidos: ", "Edad: "};
            String [] respuestas = new String[3];

            System.out.println("Responde a las siguientes preguntas, por favor: ");
            for (int i=0; i<preguntas.length;i++){
                System.out.println(preguntas[i]);

                if (i==2){
                    comprobacionInt();
                    String edad = lector.next();

                    lector.nextLine();

                    respuestas[i] = edad+" años";

                }else if (i==0){
                    respuestas[i] = lector.next();
                }else {
                    respuestas[i] = lector.next();
                    respuestas[i] += " " +lector.next();
                    lector.nextLine();
                }

            }

            PrintStream crearInfoPersona = new PrintStream(info_persona);

            for (String respuesta: respuestas){
                crearInfoPersona.println(respuesta);
            }

            crearInfoPersona.close();

            System.out.println("Información añadida al archivo.");

        }catch (Exception e){
            System.out.println("Error encontrado: " + e);
        }


        return info_persona;
    }

    public void mostrarPorPantalla(File info_persona){
        try{
            Scanner lectura_infoPersona = new Scanner(info_persona);

            while (lectura_infoPersona.hasNextLine()){
                String informacion_persona = lectura_infoPersona.nextLine();

                System.out.println(informacion_persona);
            }

        }catch (Exception e){
            System.out.println("Error: " + e);
        }
    }

    public void comprobacionInt(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número válido, vuelve a introducirlo, por favor");
            lector.next();
        }
    }
}
