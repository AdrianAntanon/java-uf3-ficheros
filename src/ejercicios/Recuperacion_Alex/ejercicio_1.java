package ejercicios.Recuperacion_Alex;

import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;

public class ejercicio_1 {

    final int SUMA = 5;

    public static void main(String[] args) {
        ejercicio_1 programa = new ejercicio_1();

        programa.inici();
    }

    public void inici(){

        File valores_enteros = new File("valores_enteros.txt");

        int contador = 1;

        try {
            Scanner lector = new Scanner(valores_enteros);
            int [] arrayNumeros = new int[10];


            while (lector.hasNextLine()){
                String valor = lector.nextLine();
                int numero = Integer.parseInt(valor);

                if (contador%2==0){
                    numero+=SUMA;
                }
                arrayNumeros[contador-1] = numero;
                contador++;
            }

            for (int numero: arrayNumeros){
                System.out.print(numero + " ");
            }

            System.out.println("\nInformación guardada con éxito, procediendo a la reescritura del archivo");

            PrintStream writer = new PrintStream(valores_enteros);

            for (int i = 0; i < 10; i++) {
                writer.println(arrayNumeros[i]);
            }

            System.out.println("Archivo tratado con éxito.\n" +
                    "Saliendo del programa");

        }catch (Exception e){
            System.out.println("Error: " + e);
        }

    }
}
