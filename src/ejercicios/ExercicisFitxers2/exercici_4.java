package ejercicios.ExercicisFitxers2;

import java.io.File;
import java.util.Scanner;

/**
 @description: Uses a file from past exercise to counts how many people is over 18 years old.
 @author: Adrian Antanyon
 @version: 30/04/2020
 */
public class exercici_4 {
    public static void main(String[] args) {
        exercici_4 programa = new exercici_4();
        programa.inici();
    }

    public void inici(){
        File personesTXT = new File("persones.txt");
        int numero_mayores_edad = 0;
        try{
            Scanner lector = new Scanner(personesTXT);
            while (lector.hasNextLine()){
                String informacion_persona = lector.nextLine();

                String [] apartados_individuales = informacion_persona.split(" ");

                if (Integer.parseInt(apartados_individuales[3])>=18){

                    numero_mayores_edad++;
                    System.out.println(informacion_persona);

                }
            }

            if (numero_mayores_edad == 0){
                System.out.println("No hay ninguna persona que tenga 18 años o más");
            }else if (numero_mayores_edad == 1){
                System.out.println("En total hay " + numero_mayores_edad + " persona mayor de edad");
            }else {
                System.out.println("En total hay " + numero_mayores_edad + " personas mayores de edad");
            }

        }catch (Exception e){
            System.out.println("Error: " + e);
        }
    }

}
