package ejercicios.ExercicisFitxers2;

import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;

/**
 @description: Creates a new file by merging the files from exercise 1
 @author: Adrian Antanyon
 @version: 27/04/2020
 */
public class exercici_2 {
    public static void main(String[] args) {
        exercici_2 programa = new exercici_2();
        programa.inici();
    }

    public void inici(){
        int [] numPares, numImpares;

        numImpares = leerNumeros("senars.txt");

        numPares = leerNumeros("parells.txt");

        if (numPares != null && numImpares!= null) {

            fusionDeArchivos("1a100.txt",numPares,numImpares);

        } else {

            System.out.println("Se ha producido un error en la lectura de los datos.");

        }
    }
    public int[] leerNumeros(String archivoTXT) {
        final int MAX_NUMEROS = 50;
        try {
            File numeros = new File(archivoTXT);

            Scanner lector = new Scanner(numeros);

            int[] datos = new int[MAX_NUMEROS];
            int i=0;

            while (lector.hasNextInt()){
                datos[i] = lector.nextInt();
                i++;
            }

            return datos;

        } catch (Exception e) {
            return null;
        }
    }

    public void fusionDeArchivos(String archivoTXT, int[] pares, int [] impares) {
        try {
            File numeros = new File(archivoTXT);
            PrintStream writer = new PrintStream(numeros);

            for (int i = 0; i < pares.length; i++) {
                writer.println(impares[i]);
                writer.println(pares[i]);
            }

            System.out.println("El archivo " + numeros.getName() + " ha sido generado con éxito");

            writer.close();

        } catch (Exception e) {
            System.out.println("Error al transcribir los datos: " + e);
        }
    }

}
