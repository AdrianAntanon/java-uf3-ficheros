package ejercicios.ExercicisFitxers2;

import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;

/**
 @description: Creates a file with array of 7x10 integer numbers, after that copy the result in another internal array and print it by screen.
 @author: Adrian Antanyon
 @version: 1/05/2020
 */
public class exercici_5 {
    public static void main(String[] args) {
        exercici_5 programa = new exercici_5();

        programa.inici();
    }

    public void inici(){
        File matriz_7x10 = new File("matriz_7x10.txt");

        crearMatriz(matriz_7x10);
        iniciarMatriz(matriz_7x10);
    }

    public void crearMatriz(File matriz){
        try{
            PrintStream crearMatriz = new PrintStream(matriz);
            System.out.println("Se ha creado " + matriz.getName() + " y está ubicado en " + matriz.getAbsolutePath());
            crearMatriz.close();
        }catch (Exception e){
            System.out.println("Error: " + e);
        }
    }

    public void iniciarMatriz(File matriz){
        int[][] matriz_7x10 =new int[7][10];

        try {
            PrintStream iniciarValoresMatriz = new PrintStream(matriz);

            for (int i=0; i<matriz_7x10.length;i++){

                for (int j=0;j<matriz_7x10[i].length;j++){

                    iniciarValoresMatriz.print((int)(Math.random() * 2) + " ");
                }
                iniciarValoresMatriz.println();
            }
            iniciarValoresMatriz.close();
            System.out.println("Se ha rellenado el archivo " + matriz.getName() + " con una matriz de 7x10 ");
            rellenarMatriz(matriz_7x10, matriz);

        }catch (Exception e){
            System.out.println("Error: " + e);
        }
    }

    public void rellenarMatriz(int [][] matriz_iniciada, File matriz){

        try {
            Scanner lector = new Scanner(matriz);

            for (int i=0; i<matriz_iniciada.length;i++){

                for (int j=0;j<matriz_iniciada[i].length;j++){
                    matriz_iniciada[i][j] = lector.nextInt();
                }
            }

            imprimirMatriz(matriz_iniciada);

        }catch (Exception e){
            System.out.println("Error: " + e);
        }
    }

    public void imprimirMatriz(int [][] matriz_llena){
        System.out.println("Lectura del archivo completado, copiando los datos esritos en una matriz de enteros interna\n" +
                "Quedaría de la siguiente forma:\n" +
                "");
        for (int i=0; i<matriz_llena.length;i++){
            System.out.print("[ ");
            for (int j=0;j<matriz_llena[i].length;j++){
                System.out.print(matriz_llena[i][j] + " ");
            }
            System.out.println("]");
        }

    }
}
