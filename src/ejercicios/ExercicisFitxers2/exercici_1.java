package ejercicios.ExercicisFitxers2;

import java.io.File;
import java.io.PrintStream;

/**
 @description: Create two files with odd or even numbers inside up to 100
 @author: Adrian Antanyon
 @version: 26/04/2020
 */
public class exercici_1 {
    public static void main(String[] args) {
        exercici_1 programa = new exercici_1();
        programa.inici();
    }

    public void inici(){
        File parells = new File("parells.txt");
        File senars = new File("senars.txt");

        try {
            PrintStream pares = new PrintStream(parells);
            PrintStream impares = new PrintStream(senars);

            escribirNum(pares, impares);

            System.out.println("Los archivos "+parells.getName() + " y " + senars.getName() + " han sido creados con éxito." );
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

    }

    public void escribirNum(PrintStream pares, PrintStream impares){
        for (int i = 1; i <= 100; i++) {
            if (i%2==0){
                pares.println(i);
            }else {
                impares.println(i);
            }
        }
        pares.close();
        impares.close();
    }

}
