package ejercicios.ExercicisFitxers2.Hector;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Ex6 {
    public static void main(String[] args) {

        File numeros =new File("numeros.txt");
        File java =new File("GestioNotes2.java");

        mostrar(numeros,java);
    }
    public static void mostrar(File numeros, File java){


        int b=1;

        try {
            Scanner num=new Scanner(numeros);
            Scanner javas=new Scanner(java);

            ArrayList<Integer> numb = new ArrayList<>();

            while(num.hasNextLine()) {
                int n=num.nextInt();
                numb.add(n);

            }

            for (int a: numb){

                while (javas.hasNextLine()){
                    String linia=javas.nextLine();
                    if (b==a) {
                        System.out.println(linia);
                        break;
                    }
                    b++;
                }
                b++;

            }

        }catch (Exception e){
            System.out.println("Error");
        }
    }
}
