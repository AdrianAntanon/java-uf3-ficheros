package ejercicios.ExercicisFitxers2;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 @description: Creates a txt file with information from different people entered by keyboard until the user stops.
 @author: Adrian Antanyon
 @version: 28/04/2020
 */
public class exercici_3 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        exercici_3 programa = new exercici_3();

        programa.inici();
    }

    public void inici(){
        File personas_txt = new File("persones.txt");
        ArrayList<String[]> informacion_persona = new ArrayList<>();
        crearArchivo(personas_txt);

        boolean menuActivo = true;
        String seleccion;

        do {
            menu();
            seleccion = lector.nextLine().toUpperCase();

            switch (seleccion){
                case "A":
                    informacion_persona = llegirPersona(personas_txt, informacion_persona);
                    break;
                case "B":
                    menuActivo = false;
                    System.out.println("Que pase un buen día, hasta la próxima!");
                    break;
                default:
                    System.out.println("Opción no valida.");
            }

        }while (menuActivo);

        escriurePersonaADisc(personas_txt,informacion_persona);

    }

    public void menu(){
        System.out.println("Seleccione una opción, por favor\n" +
                "A) Introducir datos de personal\n" +
                "B) Salir");
    }

    public void crearArchivo(File personas_txt){
        try{
            boolean crear = personas_txt.createNewFile();
            System.out.println("Archivo creado --> " + personas_txt.getAbsolutePath());
        }catch (Exception e){
            System.out.println("Error encontrado: " + e);
        }
    }

    public ArrayList<String[]> llegirPersona(File personas_txt, ArrayList<String[]> informacion_persona){

        String [] preguntas = {"Nombre: ", "Apellido: ", "NIF: ", "Edad: ", "Altura (en CM): " };
        String [] respuestas = new String[5];

        System.out.println("Introduce los datos de persona, por favor: ");
        for (int i=0; i<preguntas.length;i++){
            System.out.println(preguntas[i]);
            if (i==3){
                comprobarInt();
                int edad = lector.nextInt();

                lector.nextLine();

                respuestas[i] = edad+"";

            }else{
                respuestas[i] = lector.nextLine();
            }
        }

        informacion_persona.add(respuestas);

        return informacion_persona;
    }

    public void escriurePersonaADisc(File personas_txt, ArrayList<String[]> informacion_persona){
        try{
            PrintStream writer = new PrintStream(personas_txt);

            for (String [] lista_respuestas: informacion_persona){

                for (String respuesta: lista_respuestas){
                    writer.print(respuesta + " ");
                }

                writer.println();
            }

            writer.close();

        }catch (Exception e){
            System.out.println("Error: " + e);
        }
    }

    public void comprobarInt(){
        while (!lector.hasNextInt()){
            System.out.println("La edad solo está permitida en formato números, vuelve a introducirla por favor.");
            lector.next();
        }
    }
}
