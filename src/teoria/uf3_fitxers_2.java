package teoria;

import java.io.File;

public class uf3_fitxers_2 {
    public static void main(String[] args) {
        uf3_fitxers_2 programa = new uf3_fitxers_2();
        programa.inici();
    }
    public void inici() {
//        File temp = new File("C:\\Usuarios");
        File carpetaUsuario = new File("C:\\wamp");
        File document = new File(carpetaUsuario+"\\license.txt");
        System.out.println(carpetaUsuario.getAbsolutePath() + " existeix? " + carpetaUsuario.exists());
        mostrarEstat(carpetaUsuario);
        mostrarEstat(document);
    }
    public void mostrarEstat(File f) {
        System.out.println(f.getAbsolutePath() + " és un fitxer? " + f.isFile());
        System.out.println(f.getAbsolutePath() + " és una carpeta? " + f.isDirectory());
    }
}
