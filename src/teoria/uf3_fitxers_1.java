package teoria;

import java.io.File;

public class uf3_fitxers_1 {
    public static void main(String[] args) {
        uf3_fitxers_1 programa = new uf3_fitxers_1();
        programa.inici();
    }
    public void inici() {
    // S'inicialitzen dues rutes absolutes diferents
        File carpetaAbs = new File("C:/Users/Usuario/Desktop/Programación web");
        File fitxerAbs = new File("C:/Users/Usuario/Desktop/Programación web/document.txt");
    // I unes rutes relatives
        File carpetaRel = new File("Programación web");
        File fitxerRel = new File("Programación web/document.txt");
    // Mostrem les dades de cadascuna
        mostrarRutes(carpetaAbs);
        mostrarRutes(fitxerAbs);
        mostrarRutes(carpetaRel);
        mostrarRutes(fitxerRel);
    }
    public void mostrarRutes(File f) {
        System.out.println("La ruta és " + f.getAbsolutePath());
        System.out.println("El seu pare és " + f.getParent());
        System.out.println("El seu nom és " + f.getName() + "\n");
    }
}
