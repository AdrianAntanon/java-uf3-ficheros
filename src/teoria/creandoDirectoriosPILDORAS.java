package teoria;

import java.io.File;
import java.io.IOException;

public class creandoDirectoriosPILDORAS {
    public static void main(String[] args) throws IOException {
        File ruta_archivo = new File("C:"+File.separator+"Users"+File.separator+"Usuario"+File.separator+"Desktop"+File.separator+"Programación web"+File.separator+"CarpetaPrueba"+File.separator+"Balance primer trimestre.xlsx");

        //directorio.mkdir(); sirve para crear la carpeta si no existe

        try {
            ruta_archivo.createNewFile();
        }catch (IOException e){
            e.printStackTrace();
        }

        /*createNewFile() sirve para crear el archivo al que nos referimos siemore que no exista y esté todo correcto, es IMPRESCINDIBLE marcar siempre la extensión del archivo
        * Solo se ejecutará si lo colocamos en un bloque try catch, es decir, que esté preparado por si fallo y genera una excepción, para ello debemos importar                                                                                                        */
    }
}
