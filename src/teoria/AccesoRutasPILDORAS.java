package teoria;

import java.io.File;

public class AccesoRutasPILDORAS {
    public static void main(String[] args) {
        File directorio = new File("C:\\Users\\Usuario\\IdeaProjects\\UF3_ficheros");

        System.out.println(directorio.getAbsolutePath());
        //System.out.println(directorio.exists());
        String [] nombres = directorio.list();

        /*for (String lista: nombres){
            System.out.println(lista);
        }*/

        System.out.println("Archivos y carpetas de " + directorio.getName()+"\n");

        for (int i=0;i<nombres.length;i++){
            System.out.println(nombres[i]);

            File f= new File(directorio.getAbsolutePath(), nombres[i]);

            if (f.isDirectory()){
                System.out.println("Que contiene lo siguiente:");
                String [] archivosSubCarpeta = f.list();
                for (int j=0;j<archivosSubCarpeta.length;j++){
                    System.out.println(archivosSubCarpeta[j]);
                }
                System.out.println("----"+"\n");
            }
        }

    }
}
